package com.metapack.accumulator;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * A singleton implementation of {@link Accumulator} interface.
 * 
 */
public class AccumulatorImpl implements Accumulator {

	private AtomicInteger total = new AtomicInteger();

	private static class SingletonHolder {
		private static final AccumulatorImpl INSTANCE = new AccumulatorImpl();
	}

	public static AccumulatorImpl getInstance() {
		return SingletonHolder.INSTANCE;
	}

	// private constructor
	private AccumulatorImpl() {
		total.set(0);
	}

	/**
	 * {@inheritDoc}
	 */
	public int accumulate(int... values) {
		if (values.length == 0) {
			throw new IllegalArgumentException("Too few arguments");
		}
		int sum = 0;
		for (int arg : values) {
			sum += arg;
		}
		total.getAndAdd(sum);
		return sum;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTotal() {
		return total.get();
	}

	/**
	 * {@inheritDoc}
	 */
	public void reset() {
		total.set(0);
	}

	@Override
	public String toString() {
		return "AccumulatorImpl [total=" + total + "]";
	}

}
