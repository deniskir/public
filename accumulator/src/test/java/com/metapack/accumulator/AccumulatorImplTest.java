package com.metapack.accumulator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccumulatorImplTest {

	Accumulator accumulator = null;

	@Before
	public void setUp() {
		accumulator = AccumulatorImpl.getInstance();
	}

	@After
	public void tearDown() {
		AccumulatorImpl.getInstance().reset();
	}

	@Test
	public void testAccumulate() {
		Assert.assertEquals("Accumulated sum should be 6(six)", 6, accumulator.accumulate(1, 2, 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAccumulateNoArguments() {
		accumulator.accumulate();
	}

	@Test
	public void testGetTotal() {
		accumulator.accumulate(1, 2, 3);
		accumulator.accumulate(4);
		Assert.assertEquals("The total should be 10(ten)", 10, accumulator.getTotal());
	}

	@Test
	public void testReset() {
		accumulator.accumulate(1, 2, 3);
		accumulator.accumulate(4);
		accumulator.reset();
		Assert.assertEquals("The total should be 0(zero) after reset()", 0, accumulator.getTotal());
	}
}
